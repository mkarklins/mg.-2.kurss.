
// Autors: Magnuss Kārkliņš MK08242

import java.util.Random;

public class Domino12 {

  private static Domino[] domino_array = new Domino[91];

  public static void main( String[] args ) {
    int player_count = Integer.parseInt( args[0] );

    generate_dominos();
    shuffle_dominos();
    deal_dominos( player_count );
  }

private

  static void deal_dominos( int player_count ){
    int hand = 91 / player_count;
    int total = hand * player_count;

    for( int i = 1; i <= player_count; i++ ){
      System.out.print(i + ".spēlētājam ir: ");

      int offset = (i - 1) * hand;
      int limit = offset + hand;

      for( int j = offset; j < limit; j++ )
        System.out.print(domino_array[j].to_s() + " ");
      System.out.println("");
    }
  }

  static void generate_dominos(){
    int domino_array_iterator = 0;
    for( int x = 0; x <= 12; x++ ){
      for( int y = 0; y <= 12; y++ ){
        if( y < x )
          continue;
        domino_array[domino_array_iterator] = new Domino(x, y);
        domino_array_iterator++;
      }
    }
  }

  static void shuffle_dominos(){
    Random random_numbers = new Random();
    for ( int i = 0; i < domino_array.length; i++ ) {
      int random_position = random_numbers.nextInt(domino_array.length);
      Domino temp                    = domino_array[i];
      domino_array[i]                = domino_array[random_position];
      domino_array[random_position]  = temp;
    }
  }
}

class Domino {
  private int left, right;
  public Domino( int input_left, int input_right ){
    left = input_left;
    right = input_right;
  }
  public String to_s() { return left + "-" + right; }
}
