
// Autors: Magnuss Kārkliņš MK08242

public class MD1 {

  public static void main(String[] args) {
    int Z = Integer.parseInt(args[0]);
    int N = Integer.parseInt(args[1]);

    if (valid_attributes(Z, N))
      draw_figure(Z, N);
    else
      System.out.println("DATI NAV KOREKTI!");
  }

  private

  static void draw_figure(int Z, int N) {
    System.out.println("1234567890123456789012345678901234567890123456789012345678901234567890");
    for(int i = 0; i < Z ; i++){
      generate_offset(N + i);
      for(int j = 0; j < Z - i; j++){
        System.out.print("* ");
      }
      System.out.println();
    }
  }

  static void generate_offset(int N) {
    for(int i = 0; i < N; i++){
      System.out.print(" ");
    }
  }

  static boolean valid_attributes(int Z, int N) {
    if (0 < Z && Z < 20 && 0 < N && N < 30)
      return true;
    else
      return false;
  }
}
